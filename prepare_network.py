import torch
import torch.nn as nn
from residual_block import ResidualBlock


class OriginalPrepNetwork(nn.Module):
    def __init__(self):
        super(OriginalPrepNetwork, self).__init__()
        self.initialP3 = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=3, padding=1),
            nn.ReLU())
        self.initialP4 = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU())
        self.initialP5 = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=5, padding=2),
            nn.ReLU())
        self.finalP3 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=3, padding=1),
            nn.ReLU())
        self.finalP4 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU())
        self.finalP5 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=5, padding=2),
            nn.ReLU())

    def forward(self, p):
        p1 = self.initialP3(p)
        p2 = self.initialP4(p)
        p3 = self.initialP5(p)
        mid = torch.cat((p1, p2, p3), 1)
        p4 = self.finalP3(mid)
        p5 = self.finalP4(mid)
        p6 = self.finalP5(mid)
        out = torch.cat((p4, p5, p6), 1)
        return out


class MixedPrepNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.initialP3 = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=3, padding='same'), nn.ReLU(),
            ResidualBlock(50, 3), nn.ReLU(), ResidualBlock(50, 3), nn.ReLU(), ResidualBlock(50, 3), nn.ReLU())
        self.initialP4 = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=4, padding='same'), nn.ReLU(),
            ResidualBlock(50, 4), nn.ReLU(), ResidualBlock(50, 4), nn.ReLU(), ResidualBlock(50, 4), nn.ReLU())
        self.initialP5 = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=5, padding=2), nn.ReLU(),
            ResidualBlock(50, 5), nn.ReLU(), ResidualBlock(50, 5), nn.ReLU(), ResidualBlock(50, 5), nn.ReLU())
        self.finalP3 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=3, padding=1),
            nn.ReLU())
        self.finalP4 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU())
        self.finalP5 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=5, padding=2),
            nn.ReLU())

    def forward(self, p):
        p1 = self.initialP3(p)
        p2 = self.initialP4(p)
        p3 = self.initialP5(p)
        mid = torch.cat((p1, p2, p3), 1)
        p4 = self.finalP3(mid)
        p5 = self.finalP4(mid)
        p6 = self.finalP5(mid)
        out = torch.cat((p4, p5, p6), 1)
        return out


class DeepPrepNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.residuals = nn.Sequential(
            nn.Conv2d(3, 50, kernel_size=3, padding='same'), nn.ReLU(),
            ResidualBlock(50, 3), nn.ReLU(), ResidualBlock(
                50, 3), nn.ReLU(), ResidualBlock(50, 3), nn.ReLU(),
            ResidualBlock(50, 4), nn.ReLU(), ResidualBlock(
                50, 4), nn.ReLU(), ResidualBlock(50, 4), nn.ReLU(),
            ResidualBlock(50, 5), nn.ReLU(), ResidualBlock(
                50, 5), nn.ReLU(), ResidualBlock(50, 5), nn.ReLU()
        )

    def forward(self, p):
        return self.residuals(p)
