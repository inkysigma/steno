import torch
import torch.nn as nn
from torch.nn.modules.activation import ReLU
from residual_block import ResidualBlock


def gaussian(tensor, mean=0, stddev=0.1):
    '''Adds random noise to a tensor.'''

    noise = torch.nn.init.normal_(torch.Tensor(tensor.size()), 0, 0.1)
    return tensor + noise

# Hiding Network (5 conv layers)


class OriginalHidingNetwork(nn.Module):
    def __init__(self):
        super(OriginalHidingNetwork, self).__init__()
        self.initialH3 = nn.Sequential(
            nn.Conv2d(153, 50, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=3, padding=1),
            nn.ReLU())
        self.initialH4 = nn.Sequential(
            nn.Conv2d(153, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU())
        self.initialH5 = nn.Sequential(
            nn.Conv2d(153, 50, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=5, padding=2),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=5, padding=2),
            nn.ReLU())
        self.finalH3 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=3, padding=1),
            nn.ReLU())
        self.finalH4 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=4, padding=1),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding=2),
            nn.ReLU())
        self.finalH5 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=5, padding=2),
            nn.ReLU())
        self.finalH = nn.Sequential(
            nn.Conv2d(150, 3, kernel_size=1, padding=0))

    def forward(self, h):
        h1 = self.initialH3(h)
        h2 = self.initialH4(h)
        h3 = self.initialH5(h)
        mid = torch.cat((h1, h2, h3), 1)
        h4 = self.finalH3(mid)
        h5 = self.finalH4(mid)
        h6 = self.finalH5(mid)
        mid2 = torch.cat((h4, h5, h6), 1)
        out = self.finalH(mid2)
        out_noise = gaussian(out.data, 0, 0.1)
        return out, out_noise


class MixedHidingNetwork(nn.Module):
    def __init__(self):
        super(MixedHidingNetwork, self).__init__()
        self.initialH3 = nn.Sequential(
            nn.Conv2d(153, 50, kernel_size=3, padding='same'),
            nn.ReLU(),
            ResidualBlock(50, 3), nn.ReLU(), ResidualBlock(50, 3), nn.ReLU(), ResidualBlock(50, 3), nn.ReLU())
        self.initialH4 = nn.Sequential(
            nn.Conv2d(153, 50, kernel_size=4, padding='same'),
            nn.ReLU(),
            ResidualBlock(50, 4), nn.ReLU(), ResidualBlock(50, 4), nn.ReLU(), ResidualBlock(50, 4), nn.ReLU())
        self.initialH5 = nn.Sequential(
            nn.Conv2d(153, 50, kernel_size=5, padding='same'),
            nn.ReLU(),
            ResidualBlock(50, 5), nn.ReLU(), ResidualBlock(50, 5), nn.ReLU(), ResidualBlock(50, 5), nn.ReLU())
        self.finalH3 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=3, padding='same'),
            nn.ReLU())
        self.finalH4 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=4, padding='same'),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding='same'),
            nn.ReLU())
        self.finalH5 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=5, padding='same'),
            nn.ReLU())
        self.finalH = nn.Sequential(
            nn.Conv2d(150, 3, kernel_size=1, padding=0))

    def forward(self, h):
        h1 = self.initialH3(h)
        h2 = self.initialH4(h)
        h3 = self.initialH5(h)
        mid = torch.cat((h1, h2, h3), 1)
        h4 = self.finalH3(mid)
        h5 = self.finalH4(mid)
        h6 = self.finalH5(mid)
        mid2 = torch.cat((h4, h5, h6), 1)
        out = self.finalH(mid2)
        out_noise = gaussian(out.data, 0, 0.1)
        return out, out_noise


class DeepHidingNetwork(nn.Module):
    def __init__(self):
        super(DeepHidingNetwork, self).__init__()
        self.initialH3 = nn.Sequential(
            nn.Conv2d(6, 150, kernel_size=3, padding='same', groups=2),
            nn.ReLU(),
            ResidualBlock(150, 3, groups=2), nn.ReLU(), ResidualBlock(
                150, 3, groups=2), nn.ReLU(), ResidualBlock(150, 3), nn.ReLU(),
            nn.Conv2d(150, 50, kernel_size=3, padding='same'),
            nn.ReLU())
        self.initialH4 = nn.Sequential(
            nn.Conv2d(6, 150, kernel_size=4, padding='same', groups=2),
            nn.ReLU(),
            ResidualBlock(150, 4, groups=2), nn.ReLU(), ResidualBlock(
                150, 4, groups=2), nn.ReLU(), ResidualBlock(150, 4), nn.ReLU(),
            nn.Conv2d(150, 50, kernel_size=4, padding='same'),
            nn.ReLU())
        self.initialH5 = nn.Sequential(
            nn.Conv2d(6, 150, kernel_size=5, padding='same', groups=2),
            nn.ReLU(),
            ResidualBlock(150, 5, groups=2), nn.ReLU(), ResidualBlock(
                150, 5, groups=2), nn.ReLU(), ResidualBlock(150, 5), nn.ReLU(),
            nn.Conv2d(150, 50, kernel_size=5, padding='same'),
            nn.ReLU())
        self.finalH3 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=3, padding='same'),
            nn.ReLU())
        self.finalH4 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=4, padding='same'),
            nn.ReLU(),
            nn.Conv2d(50, 50, kernel_size=4, padding='same'),
            nn.ReLU())
        self.finalH5 = nn.Sequential(
            nn.Conv2d(150, 50, kernel_size=5, padding='same'),
            nn.ReLU())
        self.finalH = nn.Sequential(
            nn.Conv2d(150, 3, kernel_size=1, padding=0))

    def forward(self, h):
        h1 = self.initialH3(h)
        h2 = self.initialH4(h)
        h3 = self.initialH5(h)
        mid = torch.cat((h1, h2, h3), 1)
        h4 = self.finalH3(mid)
        h5 = self.finalH4(mid)
        h6 = self.finalH5(mid)
        mid2 = torch.cat((h4, h5, h6), 1)
        out = self.finalH(mid2)
        out_noise = gaussian(out.data, 0, 0.1)
        return out, out_noise
