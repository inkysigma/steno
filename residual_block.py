import torch.nn as nn
import torch.nn.functional as f
import torch


class ResidualBlock(nn.Module):
    def __init__(self, channels, kernel: int, groups: int = 1):
        super(ResidualBlock, self).__init__()
        self.conv1 = nn.Conv2d(
            channels, channels, kernel_size=kernel, groups=groups, padding='same')
        self.conv2 = nn.Conv2d(
            channels, channels, kernel_size=kernel, groups=groups, padding='same')
        self.conv3 = nn.Conv2d(
            channels, channels, kernel_size=1
        )

    def forward(self, x):
        return self.conv3(x) + self.conv2(f.relu(self.conv1(x)))


if __name__ == "__main__":
    im = torch.zeros((1, 3, 10, 10))
    block = ResidualBlock(3, 5)
    block(im)
