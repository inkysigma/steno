import numpy as np
from numba import jit
from typing import Tuple


def to_bits(b: int):
    return f'{b:08b}'


def to_int(b: str):
    return int(b, 2)


def merge_bits(b1: str, b2: str) -> str:
    return b1[:4] + b2[:4]


def separate_bits(bits: str) -> Tuple[str, str]:
    return bits[:4] + '0000', bits[4:] + '0000'


def merge(im1: np.asarray, im2: np.asarray):
    """
    Merges two images of the same shape
    """
    assert im1.shape == im2.shape
    if len(im1.shape) == 2:
        im1 = np.expand_dims(im1, axis=-1)
    if len(im2.shape) == 2:
        im2 = np.expand_dims(im2, axis=-1)
    merged = np.zeros(shape=im1.shape, dtype=int)
    for c in range(im1.shape[2]):
        for i in range(im1.shape[0]):
            for j in range(im1.shape[1]):
                merged[i, j, c] = to_int(
                    merge_bits(
                        to_bits(im1[i, j, c]),
                        to_bits(im2[i, j, c])
                    )
                )
    return merged


def unmerge(im: np.asarray):
    if len(im.shape) == 2:
        im = np.expand_dims(im, axis=-1)
    im1, im2 = np.zeros(im.shape, dtype=np.int32), np.zeros(im.shape, dtype=np.int32)
    for c in range(im1.shape[2]):
        for i in range(im1.shape[0]):
            for j in range(im1.shape[1]):
                a, b = separate_bits(to_bits(im[i, j, c]))
                im1[i, j, c] = to_int(a)
                im2[i, j, c] = to_int(b)
    return im1, im2
