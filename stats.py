import lsb
from sklearn.metrics import mean_squared_error
import glob
import random
from PIL import Image
import numpy as np

RUN_LSB = True
files = glob.glob("images/**/*.jpg")


def load_file(f):
    with Image.open(f) as im:
        return np.asarray(im)


def test(merge_func, unmerge_func, sample_count: int = 10**4):
    a = random.choices(files, k=sample_count)
    b = random.choices(files, k=sample_count)
    references = []
    recovered = []
    for i, j in zip(a, b):
        ima = load_file(i)
        imb = load_file(j)
        merged = merge_func(ima, imb)
        im1, im2 = unmerge_func(merged)
        references.append(mean_squared_error(
            ima.flatten(), im1.flatten(), squared=False))
        recovered.append(mean_squared_error(
            imb.flatten(), im2.flatten(), squared=False))
    return np.mean(references), np.mean(recovered)


if __name__ == "__main__":
    if RUN_LSB:
        print(test(lsb.merge, lsb.unmerge))
