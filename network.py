import torch
import torch.nn as nn
import torch.nn.functional as f
import prepare_network
import hiding_network
import reveal_network


def customized_loss(secret, cover, pred_secret, pred_cover, beta, loss='l2'):
    ''' Calculates loss specified on the paper.'''
    if loss == 'l2':
        loss_cover = torch.nn.functional.mse_loss(cover, pred_cover)
    else:
        loss_cover = f.l1_loss(cover, pred_cover)
    if loss == 'l2':
        loss_secret = torch.nn.functional.mse_loss(secret, pred_secret)
    else:
        loss_secret = f.l1_loss(secret, pred_secret)
    loss_all = loss_cover + beta * loss_secret
    return loss_all, loss_cover, loss_secret


# Join three networks in one module


class OriginalNet(nn.Module):
    def __init__(self):
        super(OriginalNet, self).__init__()
        self.prepare = prepare_network.OriginalPrepNetwork()
        self.hiding = hiding_network.OriginalHidingNetwork()
        self.reveal = reveal_network.OriginalRevealNetwork()

    def forward(self, secret, cover):
        x_1 = self.prepare(secret)
        mid = torch.cat((x_1, cover), 1)
        x_2, x_2_noise = self.hiding(mid)
        x_3 = self.reveal(x_2_noise)
        return x_2, x_3


class MixedNet(nn.Module):
    def __init__(self):
        super(MixedNet, self).__init__()
        self.prepare = prepare_network.MixedPrepNetwork()
        self.hiding = hiding_network.MixedHidingNetwork()
        self.reveal = reveal_network.MixedRevealNetwork()

    def forward(self, secret, cover):
        x_1 = self.prepare(secret)
        mid = torch.cat((x_1, cover), 1)
        x_2, x_2_noise = self.hiding(mid)
        x_3 = self.reveal(x_2_noise)
        return x_2, x_3


class DeepNet(nn.Module):
    def __init__(self):
        super(DeepNet, self).__init__()
        self.hiding = hiding_network.DeepHidingNetwork()
        self.reveal = reveal_network.MixedRevealNetwork()

    def forward(self, secret, cover):
        mid = torch.cat((secret, cover), 1)
        x_2, _ = self.hiding(mid)
        x_3 = self.reveal(x_2)
        return x_2, x_3


class DeepResidualNet(nn.Module):
    def __init__(self):
        super(DeepResidualNet, self).__init__()
        self.hiding = hiding_network.DeepHidingNetwork()
        self.reveal = reveal_network.MixedRevealNetwork()

    def forward(self, secret, cover):
        mid = torch.cat((secret, cover), 1)
        x_2, _ = self.hiding(mid)
        out = x_2 + cover
        x_3 = self.reveal(out)
        return out, x_3


def merge(net):
    pass


def unmerge(net):
    pass
